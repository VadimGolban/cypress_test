/// <reference types ="cupress"/>

import 'cypress-file-upload';

describe('Meinfoto-canvas', ()=>{

beforeEach(()=>{

cy.visit('https://www.meinfoto.de/foto-in-gross/foto-auf-leinwand.jsf')

})

const fileName = 'test1.jpg';

it('Should click add picture photo',()=> {
    cy.get('input.js-du-file:first').click()
    cy.fixture(fileName, 'base64').then(fileContent => {
        cy.get('input[type="file"]:first').upload(
           { fileContent, fileName, mimeType: 'image/jpg' },
           { subjectType: 'input' },
           );
   });
});
    
})

